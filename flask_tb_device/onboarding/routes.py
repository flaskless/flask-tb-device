import flask
#from pathlib import Path
import random
from tb_device_mqtt import TBDeviceMqttClient

bp = flask.Blueprint("tb_device", __name__, template_folder="templates")

@bp.route('/claim', methods = ['GET'])
def claim():

    client: TBDeviceMqttClient = flask.current_app.extensions.get('tb_mqtt_client')
    if not client or not client.is_connected():
        return "Not connected to MQTT"
    secret_key = random.randrange(10000, 99999)
    client.claim(secret_key=secret_key, duration=flask.current_app.config["TB_DEVICE_CLAIM_DURATION"]).get()    
    return(flask.render_template ("tb_device/claim.html", dev_name = flask.current_app.config["TB_DEVICE_NAME"], dev_secret = secret_key))
       