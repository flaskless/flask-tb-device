import flask
from tb_device_mqtt import TBDeviceMqttClient
from ..remote_shell import tb_remote_shell
from pathlib import Path


class FlaskTBBMqttClient(TBDeviceMqttClient):
    rpc_commands = {}

    def __init__(self, host, token=None, port=1883, quality_of_service=None, chunk_size=0,app=None):
        super().__init__(host, token, port, quality_of_service, chunk_size)
        self._remote_shell = tb_remote_shell.TBRemoteShell()
        self._app = app
        if self._app:
            self.init_app(app)

    def init_app(self,app):
        self._app = app
        app.extensions['tb_mqtt_client'] = self

    def on_rpc(self,method,*args, **kwargs):
        def inner(func):
            self.rpc_commands[method] = func
            # code functionality here
            self._app.logger.debug(f"Registered RPC for {method} -> {func}")            
            #func()
        # returning inner function   
        return inner
        

    def rpc_handler(self,request_id,request_body,*args,**kwargs):
        client = self
        method = request_body['method']
        params = request_body.get('params',{})

        # Check if it's a remote shell command
        if method in self._remote_shell.shell_commands.keys():
            return self._remote_shell.handle_rpc_request(client,request_id,request_body)
            

        command_to_run = client.rpc_commands.get(method)
        if not command_to_run:
            self._app.logger.warn(f'Unable to run command {method}, not registered')
            return
        result = None
        if not params:
            result = command_to_run()
        elif isinstance(params,dict):
            result = command_to_run(**params)
        elif isinstance(params,list):
            result = command_to_run(*params)
        elif isinstance(params,str):
            result = command_to_run(params)
        else:
            self._app.logger.warn(f'Unaccepted paramters: {params} for {method}')

        self.send_rpc_reply(req_id=request_id,resp=result)

        self._app.logger.debug(f"Handled RPC Request: method={method} kwargs={kwargs} result={result}")
    
def init_app(app:flask.Flask):

    client = FlaskTBBMqttClient(host=app.config["TB_DEVICE_HOST"], token=app.config["TB_DEVICE_TOKEN"])
    client.init_app(app)
    client.set_server_side_rpc_request_handler(FlaskTBBMqttClient.rpc_handler)