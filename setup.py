import os
from setuptools import find_packages, setup

def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths

packages = find_packages(exclude=["instance","tests.*", "tests", "examples", "deploy","docs"])
NAME='flask-tb-device'
VERSION='0.1'

setup(
    name=NAME,
    version=VERSION,
    package_data={NAME: package_files(NAME)},
    python_requires=">3.7.0",
    packages=packages,
    include_package_data=True,
    install_requires=['flask','tb-mqtt-client @ git+https://github.com/thingsboard/thingsboard-python-client-sdk.git', 'mmh3'],
    test_suite="tests",
)
